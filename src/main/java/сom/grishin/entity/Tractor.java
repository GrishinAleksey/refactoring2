package сom.grishin.entity;

import сom.grishin.enums.Orientation;
import сom.grishin.exception.TractorInDitchException;

public class Tractor {

    private final Position position = new Position(0, 0);
    private final Field field = new Field(5, 5);
    private Orientation orientation = Orientation.NORTH;

    public void move(String command) {
        if (command.equals("F")) {
            if (Math.abs(position.getX()) > field.getX() || Math.abs(position.getY()) > field.getY()) {
                throw new TractorInDitchException("Выход за рамки поля");
            }
            orientation.move(position);
        }
        if (command.equals("T")) {
            orientation = orientation.change();
        }
    }

    public int getPositionX() {
        return position.getX();
    }

    public int getPositionY() {
        return position.getY();
    }

    public Orientation getOrientation() {
        return orientation;
    }

}
