package сom.grishin.entity;

public class Field {

    private final Integer x;
    private final Integer y;

    public Field(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

}
