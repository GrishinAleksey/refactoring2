package сom.grishin.enums;

import сom.grishin.entity.Position;

public enum Orientation {

    NORTH {
        @Override
        public void move(Position position) {
            position.setX(position.getX() + 1);
        }
    }, WEST {
        @Override
        public void move(Position position) {
            position.setY(position.getY() - 1);
        }
    }, SOUTH {
        @Override
        public void move(Position position) {
            position.setX(position.getX() - 1);
        }
    }, EAST {
        @Override
        public void move(Position position) {
            position.setY(position.getY() + 1);
        }
    };

    public abstract void move(Position position);

    public Orientation change() {
        if (this == NORTH) return WEST;
        if (this == WEST) return SOUTH;
        if (this == SOUTH) return EAST;
        if (this == EAST) return NORTH;
        return this;
    }
}
